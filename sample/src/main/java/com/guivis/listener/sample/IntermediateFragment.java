package com.guivis.listener.sample;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.guivis.listener.Listener;

public abstract class IntermediateFragment extends BaseFragment {
    public interface BloopListener {
        void onBloop(String text);
    }

    @Listener
    BloopListener mBloopListener;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mBloopListener.onBloop("bloop");
    }
}

package com.guivis.listener.sample;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import blade.Blade;
import blade.I;
import butterknife.ButterKnife;
import butterknife.OnClick;


@Blade
public final class ListenerMainActivity
        extends AppCompatActivity {

    @OnClick(R.id.btn_1)
    void showExample1() {
        I.startParentActivityListenerActivity(this);
    }

    @OnClick(R.id.btn_2)
    void showExample2() {
        I.startParentFragmentListenerActivity(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }
}

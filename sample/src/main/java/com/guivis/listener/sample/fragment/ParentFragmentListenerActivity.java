package com.guivis.listener.sample.fragment;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.guivis.listener.sample.R;

import blade.Blade;
import blade.F;
import butterknife.ButterKnife;


@Blade
public class ParentFragmentListenerActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parent_fragment_listener);
        ButterKnife.bind(this);

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.frag, F.newParentFragmentListenerFragment())
                    .commit();
        }
    }
}

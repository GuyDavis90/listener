package com.guivis.listener.sample.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;

import com.guivis.listener.sample.ControlFragment;
import com.guivis.listener.sample.IntermediateFragment;
import com.guivis.listener.sample.R;

import blade.Blade;
import blade.F;
import butterknife.BindView;
import butterknife.ButterKnife;

@Blade
public class ParentActivityListenerActivity extends AppCompatActivity implements IntermediateFragment.BloopListener,  ControlFragment.TextChangedListener {
    @BindView(R.id.text_output)
    TextView mTextOutput;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_listener_parent);
        ButterKnife.bind(this);

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.frag, F.newControlFragment())
                    .commit();
        }
    }

    @Override
    public void onTextChanged(String text) {
        mTextOutput.setText(text);
    }

    @Override
    public void onBloop(String text) {
        Log.e("Bloop", "Bloop!");
    }
}

package com.guivis.listener.sample;

import android.text.Editable;

import com.guivis.listener.Listener;

import blade.Blade;
import butterknife.OnTextChanged;


@Blade
public class ControlFragment extends IntermediateFragment {
    public interface TextChangedListener {
        void onTextChanged(String text);
    }

    @Listener
    TextChangedListener mTextChangedListener;

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_control;
    }

    @OnTextChanged(value = R.id.text_input, callback = OnTextChanged.Callback.AFTER_TEXT_CHANGED)
    void afterTextChanged(Editable editable) {
        mTextChangedListener.onTextChanged(editable.toString());
    }
}

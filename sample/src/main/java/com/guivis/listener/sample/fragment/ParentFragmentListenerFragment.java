package com.guivis.listener.sample.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.guivis.listener.sample.ControlFragment;
import com.guivis.listener.sample.IntermediateFragment;
import com.guivis.listener.sample.R;

import blade.Blade;
import blade.F;
import butterknife.BindView;
import butterknife.ButterKnife;


@Blade
public class ParentFragmentListenerFragment extends Fragment implements IntermediateFragment.BloopListener, ControlFragment.TextChangedListener {
    @BindView(R.id.text_output)
    TextView mTextOutput;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.layout_listener_parent, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        getChildFragmentManager().beginTransaction()
                .replace(R.id.frag, F.newControlFragment())
                .commit();
    }

    @Override
    public void onTextChanged(String text) {
        mTextOutput.setText(text);
    }

    @Override
    public void onBloop(String text) {
        Log.e("Bloop", "Bloop!");
    }
}

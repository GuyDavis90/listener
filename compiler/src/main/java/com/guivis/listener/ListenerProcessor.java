package com.guivis.listener;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.Fragment;

import com.squareup.javapoet.ClassName;
import com.squareup.javapoet.JavaFile;
import com.squareup.javapoet.MethodSpec;
import com.squareup.javapoet.ParameterizedTypeName;
import com.squareup.javapoet.TypeName;
import com.squareup.javapoet.TypeSpec;
import com.squareup.javapoet.TypeVariableName;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;

import eu.f3rog.blade.compiler.builder.annotation.WeaveBuilder;
import eu.f3rog.blade.compiler.builder.annotation.WeaveIntoBuilder;
import eu.f3rog.blade.compiler.util.ProcessorUtils;

@SupportedAnnotationTypes({"com.guivis.listener.Listener"})
@SupportedSourceVersion(SourceVersion.RELEASE_7)
public class ListenerProcessor extends AbstractProcessor {
    private static final String METHOD_NAME_ATTACH = "attach";
    private static final String METHOD_NAME_DETACH = "detach";

    private Map<TypeElement, List<VariableElement>> mEnclosingsToListenersMap = new HashMap<>();

    @Override
    public synchronized void init(ProcessingEnvironment processingEnvironment) {
        super.init(processingEnvironment);
        ProcessorUtils.setProcessingEnvironment(processingEnvironment);
    }

    @Override
    public boolean process(Set<? extends TypeElement> set, RoundEnvironment roundEnvironment) {
        if (set.isEmpty()) {
            return false;
        }

        generateListenerHelperClass();

        for (Element element : roundEnvironment.getElementsAnnotatedWith(Listener.class)) {
            TypeElement enclosing = (TypeElement) element.getEnclosingElement();
            if (ProcessorUtils.isFragmentSubClass(enclosing)) {
                List<VariableElement> listenersForEnclosing = mEnclosingsToListenersMap.get(enclosing);
                if (listenersForEnclosing == null) {
                    listenersForEnclosing = new ArrayList<>();
                    mEnclosingsToListenersMap.put(enclosing, listenersForEnclosing);
                }
                listenersForEnclosing.add((VariableElement) element);
            } else {
                System.out.println("ERROR! Only Fragment subclass can contain @" + Listener.class.getSimpleName() + ".");
            }
        }

        for (Map.Entry<TypeElement, List<VariableElement>> entry : mEnclosingsToListenersMap.entrySet()) {
            TypeElement enclosing = entry.getKey();
            ClassName className = ClassName.get(enclosing);
            List<VariableElement> listeners = entry.getValue();

            System.out.println("Starting processing for : " + className.simpleName() + "_Listeners");

            TypeSpec.Builder builder = TypeSpec.classBuilder(className.simpleName() + "_Listeners");

            builder.addAnnotation(WeaveIntoBuilder.buildFor(className));

            addAttachMethod(builder, className, listeners);
            addDetachMethod(builder, className, listeners);

            JavaFile javaFile = JavaFile.builder(className.packageName(), builder.build()).build();
            try {
                javaFile.writeTo(processingEnv.getFiler());
                System.out.println("Generated Class File: " + className.simpleName() + "_Listeners");
            } catch (IOException e) {
                // Note: calling e.printStackTrace() will print IO errors
                // that occur from the file already existing after its first run, this is normal
            }
        }

        return true;
    }

    private void addAttachMethod(TypeSpec.Builder builder, ClassName className, List<VariableElement> listeners) {
        System.out.print("adding onAttach() for ");
        for (VariableElement listener : listeners) {
            TypeName listenerClassName = ClassName.get(listener.asType());
            System.out.print(listenerClassName + " ");
        }
        System.out.print("\n");

        String target = "target";
        String activity = "activity";
        MethodSpec.Builder method = MethodSpec.methodBuilder(METHOD_NAME_ATTACH)
                .addAnnotation(WeaveBuilder.weave().method("onAttach", Activity.class)
                        .placed(WeaveBuilder.MethodWeaveType.AFTER_SUPER)
                        .withStatement("%s.%s(this, $1);", ProcessorUtils.fullName(className) + "_Listeners", METHOD_NAME_ATTACH)
                        .build())
                .addModifiers(Modifier.PUBLIC, Modifier.STATIC);

        ProcessorUtils.addClassAsParameter(method, className, target);
        method.addParameter(Activity.class, activity);

        ClassName listenerHelper = ClassName.get("com.guivis.listener", "ListenerHelper");
        for (VariableElement listener : listeners) {
            String listenerName = listener.getSimpleName().toString();
            String fieldName = ClassName.get(listener.asType()).toString();
            method.addStatement("$N.$N = $T.getListener($N, $N.class, $N)", target, listenerName, listenerHelper, target, fieldName, activity);
        }

        builder.addMethod(method.build());
    }

    private void addDetachMethod(TypeSpec.Builder builder, ClassName className, List<VariableElement> listeners) {
        System.out.print("adding onDetach() for ");
        for (VariableElement listener : listeners) {
            TypeName listenerClassName = ClassName.get(listener.asType());
            System.out.print(listenerClassName + " ");
        }
        System.out.print("\n");

        String target = "target";
        MethodSpec.Builder method = MethodSpec.methodBuilder(METHOD_NAME_DETACH)
                .addAnnotation(WeaveBuilder.weave().method("onDetach")
                        .placed(WeaveBuilder.MethodWeaveType.BEFORE_SUPER)
                        .withStatement("%s.%s(this);", ProcessorUtils.fullName(className) + "_Listeners", METHOD_NAME_DETACH)
                        .build())
                .addModifiers(Modifier.PUBLIC, Modifier.STATIC);

        ProcessorUtils.addClassAsParameter(method, className, target);

        for (VariableElement listener : listeners) {
            String listenerName = listener.getSimpleName().toString();
            method.addStatement("$N.$N = null", target, listenerName);
        }

        builder.addMethod(method.build());
    }

    private void generateListenerHelperClass() {
        System.out.println("Generating ListenerHelper class");

        TypeSpec.Builder builder = TypeSpec.classBuilder("ListenerHelper").addModifiers(Modifier.PUBLIC);

        TypeVariableName genericType = TypeVariableName.get("T", Object.class);
        ParameterizedTypeName genericClass = ParameterizedTypeName.get(ClassName.get(Class.class), genericType);

        String objectParameterName = "o";
        String clazzParameterName = "clazz";

        String fragmentParameterName = "fragment";
        String contextParameterName = "context";

        String parentFragmentLocalName = "parentFragment";
        String listenerLocalName = "listener";

        /*
        public static <T> T convertInstanceOfObject(Object o, Class<T> clazz) {
            try {
                return clazz.cast(o);
            } catch (ClassCastException e) {
                return null;
            }
        }
        */
        MethodSpec convertInstanceOfObject = MethodSpec
                .methodBuilder("convertInstanceOfObject")
                .addModifiers(Modifier.PUBLIC, Modifier.STATIC)
                .addTypeVariable(genericType)
                .returns(genericType)
                .addParameter(TypeName.OBJECT, objectParameterName)
                .addParameter(genericClass, clazzParameterName)
                .beginControlFlow("try")
                .addStatement("return $N.cast($N)", clazzParameterName, objectParameterName)
                .nextControlFlow("catch ($T e)", ClassCastException.class)
                .addStatement("return null")
                .endControlFlow()
                .build();
        builder.addMethod(convertInstanceOfObject);

        /*
        public static <T> T getListener(Fragment fragment, Class<T> clazz, Context context) {
            Fragment parentFragment = fragment.getParentFragment();
            while (parentFragment != null) {
                T listener = convertInstanceOfObject(parentFragment, clazz);
                if (listener != null) {
                    return listener;
                }
                parentFragment = parentFragment.getParentFragment();
            }

            T listener = convertInstanceOfObject(context, clazz);
            if (listener != null) {
                return listener;
            }

            // TODO: Check @Nullable
            throw new RuntimeException(fragment.getClass().getSimpleName() + " attached to invalid listener (Activity/ParentFragment). Expecting " + clazz.getSimpleName());
        }
        */
        MethodSpec getListener = MethodSpec
                .methodBuilder("getListener")
                .addModifiers(Modifier.PUBLIC, Modifier.STATIC)
                .addTypeVariable(genericType)
                .returns(genericType)
                .addParameter(Fragment.class, fragmentParameterName)
                .addParameter(genericClass, clazzParameterName)
                .addParameter(Context.class, contextParameterName)
                .addStatement("$T $N = $N.getParentFragment()", Fragment.class, parentFragmentLocalName, fragmentParameterName)
                .beginControlFlow("while ($N != null)", parentFragmentLocalName)
                .addStatement("$T $N = $N($N, $N)", genericType, listenerLocalName, convertInstanceOfObject, parentFragmentLocalName, clazzParameterName)
                .beginControlFlow("if ($N != null)", listenerLocalName)
                .addStatement("return $N", listenerLocalName)
                .endControlFlow()
                .addStatement("$N = $N.getParentFragment()", parentFragmentLocalName, parentFragmentLocalName)
                .endControlFlow()

                .addStatement("$T $N = $N($N, $N)", genericType, listenerLocalName, convertInstanceOfObject, contextParameterName, clazzParameterName)
                .beginControlFlow("if ($N != null)", listenerLocalName)
                .addStatement("return $N", listenerLocalName)
                .endControlFlow()
                // TODO: Check @Nullable
                .addStatement("throw new RuntimeException($N.getClass().getSimpleName() + \" attached to invalid listener (Activity/ParentFragment). Expecting \" + $N.getSimpleName())", fragmentParameterName, clazzParameterName)

                .build();
        builder.addMethod(getListener);

        JavaFile javaFile = JavaFile.builder("com.guivis.listener", builder.build()).build();
        try {
            javaFile.writeTo(processingEnv.getFiler());
            System.out.println("Generated ListenerHelper Class");
        } catch (IOException e) {
            // Note: calling e.printStackTrace() will print IO errors
            // that occur from the file already existing after its first run, this is normal
        }
    }
}
